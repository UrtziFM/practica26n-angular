import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExampleAComponent } from './components/example-a/example-a.component';
import { ExampleBComponent } from './components/example-b/example-b.component';


const routes: Routes = [
  { path: '', component: ExampleAComponent },
  { path: '', component: ExampleBComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
 }
